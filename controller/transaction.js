const transactionService = require('../service/transaction');

const addPerson = async (req, res, next) => {
    const result = await transactionService.addPerson(
        req.body.age,
        req.body.name,
        req.body.fatherName,
        req.body.location
    );
    return res.status(200).json({ success: true, message: '', data: result });
};

const getPerson = async (req, res, next) => {
    const personInfo = await transactionService.getPerson(req.params.id);
    return res.status(200).json({ success: true, message: '', data: personInfo });
};

const getAllPersons = async (req, res, next) => {
    const allPersonsInfo = await transactionService.getAllPersonInfo();
    return res.status(200).json({ success: true, message: '', data: allPersonsInfo });
};

const updateLocation = async (req, res, next) => {
    const result = await transactionService.updateLocation(req.params.id, req.body.location);
    return res.status(200).json({ success: true, message: '', data: result });
};

const getPersonCount = async (req, res, next) => {
    const personCount = await transactionService.countPerson();
    return res.status(200).json({ success: true, message: '', data: personCount });
};

module.exports = {
    addPerson,
    getPerson,
    getAllPersons,
    updateLocation,
    getPersonCount
};
