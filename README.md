## Smart Contract Information

* [Smart Contract](https://ropsten.etherscan.io/address/0xb15822953436D4c7a7d26B1B93D5E8CD1c84b0a8)
* [Admin Account](https://ropsten.etherscan.io/address/0x2fefa5a633a5647f9124e4e0db56f811680aa5af)
* Smart contract is found in `/resource/` directory
* Using remote [INFURA](https://infura.io/) node.
* Postman API [Doc](https://documenter.getpostman.com/view/1729081/RzZFBG7H)

## Running project

You need to have installed Node.js v10.12.0, npm v6.4.1 

### Install dependencies

To install dependencies enter project folder and run following command:
```
npm install
```

### Run server

To run server execute:
```
npm start
```

Now Go to `http://localhost:3000/`

### Make Requests

#### Person CRUD operations
1. User Operations
   - POST /api/person – create new person, parameters: age, name, fatherName, location
   * If persons age is less than 18 years, it will throw an error
   * If there is already a person with the same father name, it will throw an error

   ```json
   POST http://localhost:3000/api/person
   ```
   ```json
   {
     "age": "23",
     "name": "dummyName",
     "fatherName": "dummyFatherName",
     "location": "dummyLocation"
   }
   ```
   - GET /api/person/:id – get a person
   ```
   GET http://localhost:1350/api/person/1
   ```
   - GET /api/person – get all persons
   ```
   GET http://localhost:3000/api/person
   ```
   - GET /api/person/count – get number of persons
   ```
   GET http://localhost:3000/api/person/count
   ```
   - PATCH /api/person/:id – update person location, parameters: location
   ```json
   PUT http://localhost:1350/users/5965e3b006ae726cd6ba2ec7
   ```
   ```json
   {
     "location": "updatedLocation"
   }
   ```

## Acknowledgments

* Contract is deployed to [Ropsten](https://github.com/ethereum/ropsten) public test network 
* Using fake ether from [Faucet](https://faucet.metamask.io/) 
