const Web3 = require('web3');
const url = 'https://ropsten.infura.io/';
const address = '0x2FeFa5A633a5647F9124e4E0db56F811680aa5Af';
// contractAddress = 0xb15822953436D4c7a7d26B1B93D5E8CD1c84b0a8
// Infura address https://ropsten.infura.io/v3/92c2bb97d8484dd9ba0d01ce7219c62b
// const address = '0x0318247CB34f134f3cF49E97647227dc2D75Abe8';

const web3 = new Web3(url);
web3.eth.getBalance(address, (err, bal) => {
    if (err) console.log(err);
    console.log('Balance: ' + bal);
    console.log(web3.utils.fromWei(bal, 'ether'));
});

// const testnet = 'https://ropsten.infura.io/';
// const walletAddress = '0x8690F1feff62008A396B31c2C3f380bD0Ca6d8b8';

// const web3 = new Web3(new Web3.providers.HttpProvider(testnet));
// var balance = web3.eth.getBalance(walletAddress, (err, bal) => {
//     console.log(bal);
// }); //Will give value in.
