const Web3 = require('web3');
const web3 = new Web3('http://localhost:7545');

const account1 = '0x59048A2De25E1562EA3AD23e7b90c12A453140c8';
const account2 = '0x1b91005F5BeCa3d1ae8589797b1a81017f22C283';

const getBalance = async (accountAddress) => {
    const balance = await web3.eth.getBalance(accountAddress);
    console.log(balance);
};

const transferEther = async (fromAccount, toAccount, value) => {
    const result = await web3.eth.sendTransaction({
        from: fromAccount,
        to: toAccount,
        value: web3.utils.toWei(value, 'ether')
    });
    console.log(result);
}

// getBalance(account1);
transferEther(account1, account2, '1');
