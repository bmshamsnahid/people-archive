const Tx = require('ethereumjs-tx');
const Web3 = require('web3')

const web3 = new Web3('https://ropsten.infura.io/v3/92c2bb97d8484dd9ba0d01ce7219c62b');

const account= {
    address: '0x69411d58Ae17A87b7a377B3A62503a537F9cC7f5',
    privateKey: Buffer.from('a0958e47a35355b47e0c942bdc71bb1a4329952bb10f9d374d52b8a4597b4435', 'hex'),
};

const contractAddress = '0xb15822953436D4c7a7d26B1B93D5E8CD1c84b0a8';
const contractABI = [{"constant":true,"inputs":[],"name":"personCount","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_age","type":"uint256"},{"name":"_name","type":"string"},{"name":"_fatherName","type":"string"},{"name":"_location","type":"string"}],"name":"addPerson","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":false,"inputs":[{"name":"id","type":"uint256"},{"name":"_location","type":"string"}],"name":"updatePersonLocation","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"personsMap","outputs":[{"name":"age","type":"uint256"},{"name":"name","type":"string"},{"name":"fatherName","type":"string"},{"name":"location","type":"string"},{"name":"id","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getPersonInfo","outputs":[{"name":"","type":"uint256"},{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"}];
const contract = new web3.eth.Contract(contractABI, contractAddress);


const getBalance = async (account) => {
    const balance = await web3.eth.getBalance(account.address);
    console.log(account.address + ': ' + web3.utils.fromWei(balance,'ether'));
};

const addPerson = async () => {
    const data = contract.methods.addPerson(22, 'w3name', 'w3fname', 'w3location').encodeABI();
    web3.eth.getTransactionCount(account.address, (err, txCount) => {
        // create transaction object
        const txObject = {
            nonce: web3.utils.toHex(txCount),
            gasLimit: web3.utils.toHex(800000),
            gasPrice: web3.utils.toHex(20* 1e9),
            to: contractAddress,
            data: data,
        };
    
        // sign the transaction
        const tx = new Tx(txObject);
        tx.sign(account.privateKey);

        const serializeTx = tx.serialize();
        const raw = '0x' + serializeTx.toString('hex');

        //broadcast the transaction
        web3.eth.sendSignedTransaction(raw, (err, txHash) => {
            if(err) console.log(err);
            console.log(txHash);
        });
    });
};

const updateLocation = async (id, location) => {
    const data = contract.methods.updatePersonLocation(id, location).encodeABI();
    web3.eth.getTransactionCount(account.address, (err, txCount) => {
        // create transaction object
        const txObject = {
            nonce: web3.utils.toHex(txCount),
            gasLimit: web3.utils.toHex(800000),
            gasPrice: web3.utils.toHex(20* 1e9),
            to: contractAddress,
            data: data,
        };
    
        // sign the transaction
        const tx = new Tx(txObject);
        tx.sign(account.privateKey);

        const serializeTx = tx.serialize();
        const raw = '0x' + serializeTx.toString('hex');

        //broadcast the transaction
        web3.eth.sendSignedTransaction(raw, (err, txHash) => {
            if(err) console.log(err);
            console.log(txHash);
            return txHash;
        });
    });
};

const getPerson = async (id) => {
    const result = await contract.methods.getPersonInfo(id).call();
    return result;
};

const countPerson = async () => {
    const numberOfPerson = await contract.methods.personCount().call();
    return numberOfPerson;
};

const getAllPersonInfo = async () => {
    let personInfos = [];
    const personCount = await countPerson();
    for (let index=0; index<personCount; index++) {
        const personInfo = await getPerson(index);
        personInfos.push(personInfo);
    };
    console.log(personInfos);
    return personInfos;
};

// addPerson();
// getBalance(account);
// getPerson(2);
// updateLocation(2, 'w3 updated location');
// countPerson();
getAllPersonInfo();