const Web3 = require('web3');
const url = 'http://127.0.0.1:7545/';
const address = '0x59048A2De25E1562EA3AD23e7b90c12A453140c8';

const web3 = new Web3(url);
web3.eth.getBalance(address, (err, bal) => {
    if (err) console.log(err);
    console.log('Balance: ' + bal);
    console.log(web3.utils.fromWei(bal, 'ether'));
});
