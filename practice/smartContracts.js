const Web3 = require('web3');
const web3 = new Web3('https://ropsten.infura.io/v3/92c2bb97d8484dd9ba0d01ce7219c62b');
const contractAddress = '0xb15822953436D4c7a7d26B1B93D5E8CD1c84b0a8';
const abi = [{"constant":true,"inputs":[],"name":"personCount","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_age","type":"uint256"},{"name":"_name","type":"string"},{"name":"_fatherName","type":"string"},{"name":"_location","type":"string"}],"name":"addPerson","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":false,"inputs":[{"name":"id","type":"uint256"},{"name":"_location","type":"string"}],"name":"updatePersonLocation","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"personsMap","outputs":[{"name":"age","type":"uint256"},{"name":"name","type":"string"},{"name":"fatherName","type":"string"},{"name":"location","type":"string"},{"name":"id","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getPersonInfo","outputs":[{"name":"","type":"uint256"},{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"}];
const contract  = new web3.eth.Contract(abi, contractAddress);
contract.methods.getPersonInfo(2).call((err, result) => {
    if(err) console.log(err);
    else console.log(result);
});

contract.methods.personCount().call((err, result) => {
    if (err) console.log(err);
    else console.log(result);
});

// contract.methods.addPerson(33, "remote name", "remote Father Name", "remote location").call((err, result) => {
//     if(err) console.log(err);
//     else console.log(result);
// });
