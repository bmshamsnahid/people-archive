const Tx = require('ethereumjs-tx');
const Web3 = require('web3');

const web3 = new Web3('https://ropsten.infura.io/v3/92c2bb97d8484dd9ba0d01ce7219c62b');

const account1= {
    address: '0x69411d58Ae17A87b7a377B3A62503a537F9cC7f5',
    privateKey: Buffer.from('a0958e47a35355b47e0c942bdc71bb1a4329952bb10f9d374d52b8a4597b4435', 'hex'),
};

const account2 = {
    address: '0x7Dd78ecfebbF48113A271AcE65647204BB0f409D',
    privateKey: Buffer.from('739d84e1d7bad22c369d0344ee1e403852e9f554dbc4a0979f5b209a0ab7537b', 'hex')
};

const getBalance = async (account) => {
    const balance = await web3.eth.getBalance(account.address);
    console.log(account.address + ': ' + web3.utils.fromWei(balance,'ether'));
};

const getTransactionCount = async (account) => {
    const count = await web3.eth.getTransactionCount(account.address);
    return count;
};

// build the transaction
const buildTransactionObject = async (account1, account2, transactionCount) => {
    const transactionObject = {
        nonce: web3.utils.toHex(transactionCount),
        to: account2.address,
        value: web3.utils.toHex(web3.utils.toWei('1', 'ether')),
        gasLimit: web3.utils.toHex(21000),
        gasPrice: web3.utils.toHex(web3.utils.toWei('10', 'gwei'))
    };
    return transactionObject;
}

// sign in transaction
const signTransaction = async (account, transactionObject) => {
    const tx = new Tx(transactionObject);
    await tx.sign(account.privateKey);
    const serializedTransaction = tx.serialize();
    const raw = '0x' + serializedTransaction.toString('hex');
    return raw;
}

// broadcast the transaction
const sendSignedTransaction = async (account, raw) => {
    const txHash = await web3.eth.sendSignedTransaction(raw);
    return txHash;
};

const performTransaction = async (account1, account2) => {
    const txCount = await getTransactionCount(account1);
    // we can fix the amount of gas and gas limit here
    const txObject = await buildTransactionObject(account1, account2, txCount); 
    const raw = await signTransaction(account1, txObject);
    const txHash = await sendSignedTransaction(account1, raw);
    console.log(txHash);
};

// performTransaction(account1, account2);
// getBalance(account1);
// getBalance(account2);

// web3.eth.getTransactionCount(account1.address, async (err, txCount) => {
    
//     // build the transaction
//     const txObject = {
//         nonce: web3.utils.toHex(txCount),
//         to: account2.address,
//         value: web3.utils.toHex(web3.utils.toWei('1', 'ether')),
//         gasLimit: web3.utils.toHex(21000),
//         gasPrice: web3.utils.toHex(web3.utils.toWei('10', 'gwei'))
//     };

//     // sign the transaction
//     const tx = new Tx(txObject);
//     await tx.sign(account1.privateKey);
//     // await tx.sign(Buffer.from('a0958e47a35355b47e0c942bdc71bb1a4329952bb10f9d374d52b8a4597b4435', 'hex'));
//     const serializedTransaction = tx.serialize();
//     const raw = '0x' + serializedTransaction.toString('hex');
    
//     // broadcast the transaction    
//     web3.eth.sendSignedTransaction(raw, (err, txHash) => {
//         if (err) console.log(err);
//         else console.log('txHash: ' + txHash);
//     });
// });