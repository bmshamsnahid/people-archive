const express = require('express');
const router = express.Router();
const transactionController = require('../controller/transaction');

router.post('/', transactionController.addPerson);
router.get('/', transactionController.getAllPersons);
router.get('/count', transactionController.getPersonCount);
router.get('/:id', transactionController.getPerson);
router.patch('/:id', transactionController.updateLocation);

module.exports = router;
